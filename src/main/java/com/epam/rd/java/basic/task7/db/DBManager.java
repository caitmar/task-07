package com.epam.rd.java.basic.task7.db;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

	private static DBManager instance;

	public static synchronized DBManager getInstance() {
		if (instance == null) {
			try {
				instance = new DBManager();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return instance;
	}

	private DBManager() throws SQLException {

	}

	private Connection getConnection() throws SQLException {
		Properties properties = new Properties();
		try (InputStream inputStream = Files.newInputStream(Paths.get("app.properties"))) {
			properties.load(inputStream);
		} catch (IOException e) {
			e.printStackTrace();
		}
		String url = properties.getProperty("connection.url");
		String user = properties.getProperty("user");
		String password = properties.getProperty("password");

		return DriverManager.getConnection(url, user, password);
	}

	public List<User> findAllUsers() throws DBException {
		List<User> users = new ArrayList<>();
		String selectAllUsers = "SELECT * FROM users";
		Connection connection = null;
		Statement statement = null;
		try {
			connection = getConnection();
			connection.setAutoCommit(false);
			statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery(selectAllUsers);
			while (resultSet.next()) {
				User user = new User();
				user.setId(resultSet.getInt(1));
				user.setLogin(resultSet.getString(2));
				users.add(user);
			}
			connection.commit();
		} catch (SQLException throwables) {
			throwables.printStackTrace();
			rollBack(connection);
			throw new DBException("Mistake in finding all users", throwables);
		} finally {
			close(statement);
			close(connection);
		}
		return users;
	}

	private void rollBack(Connection connection) {
		try {
			connection.rollback();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private void close(AutoCloseable statement) {
		if (statement != null) {
			try {
				statement.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public boolean insertUser(User user) throws DBException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		String insertUser = "INSERT INTO users (login) VALUES (?)";
		try {
			connection = getConnection();
			preparedStatement = connection.prepareStatement(insertUser, PreparedStatement.RETURN_GENERATED_KEYS);
			preparedStatement.setString(1, user.getLogin());
			int count = preparedStatement.executeUpdate();
			if (count > 0) {
				ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
				if (generatedKeys.next()) {
					user.setId(generatedKeys.getInt(1));
					return true;
				}
			}
		} catch (SQLException throwables) {
			throwables.printStackTrace();
		} finally {
			close(preparedStatement);
			close(connection);
		}
		return false;
	}

	public boolean deleteUsers(User... users) throws DBException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		String deleteUsers = "DELETE FROM users WHERE login =?";
		try {
			connection = getConnection();
			preparedStatement = connection.prepareStatement(deleteUsers);
			int k = 0;
			for (User user : users) {
				preparedStatement.setString(1,user.getLogin());
				int i = preparedStatement.executeUpdate();
				k +=i;
			}
			if(k > 0){
				return true;
			}
		} catch (SQLException throwables) {
			throwables.printStackTrace();
		} finally {
			close(preparedStatement);
			close(connection);
		}
		return false;
	}

	public User getUser(String login) throws DBException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		String getUser = "SELECT * FROM users where login=?";
		User user = new User();
		try {
			connection = getConnection();
			preparedStatement = connection.prepareStatement(getUser);
			preparedStatement.setString(1, login);
			ResultSet resultSet = preparedStatement.executeQuery();
			resultSet.next();
			user.setId(resultSet.getInt(1));
			user.setLogin(resultSet.getString(2));
		} catch (SQLException throwables) {
			throwables.printStackTrace();
		} finally {
			close(preparedStatement);
			close(connection);
		}
		return user;
	}

	public Team getTeam(String name) throws DBException {
		Team team = new Team();
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		String getTeam = "SELECT * FROM teams WHERE name=?";
		try {
			connection = getConnection();
			preparedStatement = connection.prepareStatement(getTeam);
			preparedStatement.setString(1, name);
			ResultSet resultSet = preparedStatement.executeQuery();
			resultSet.next();
			team.setId(resultSet.getInt(1));
			team.setName(resultSet.getString(2));
			return team;
		} catch (SQLException throwables) {
			throwables.printStackTrace();
			throw new DBException("Team can not be found", throwables);
		} finally {
			close(preparedStatement);
			close(connection);
		}
	}
	public List<Team> findAllTeams() throws DBException {
		List<Team> teams = new ArrayList<>();
		String selectAllUsers = "SELECT * FROM teams";
		Connection connection = null;
		Statement statement = null;
		try {
			connection = getConnection();
			connection.setAutoCommit(false);
			statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery(selectAllUsers);
			while (resultSet.next()) {
				Team team = new Team();
				team.setId(resultSet.getInt(1));
				team.setName(resultSet.getString(2));
				teams.add(team);
			}
			connection.commit();
		} catch (SQLException throwables) {
			throwables.printStackTrace();
			rollBack(connection);
			throw new DBException("Mistake in finding all users", throwables);
		} finally {
			close(statement);
			close(connection);
		}
		return teams;
	}

	public boolean insertTeam(Team team) throws DBException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		String insertTeam = "INSERT INTO teams (name) VALUES (?)";
		try {
			connection = getConnection();
			preparedStatement = connection.prepareStatement(insertTeam, PreparedStatement.RETURN_GENERATED_KEYS);
			preparedStatement.setString(1, team.getName());
			int count = preparedStatement.executeUpdate();
			if (count > 0) {
				try (ResultSet generatedKeys = preparedStatement.getGeneratedKeys()) {
					if (generatedKeys.next()) {
						team.setId(generatedKeys.getInt(1));
						return true;
					}
				}
			}
		} catch (SQLException throwables) {
			throwables.printStackTrace();
			throw new DBException("can not insert this user", throwables);
		} finally {
			close(preparedStatement);
			close(connection);
		}
		return false;
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		String insertTeamsForUser = "INSERT INTO users_teams (user_id, team_id) VALUES (?,?)";
		try {
			connection = getConnection();
			connection.setAutoCommit(false);
			preparedStatement = connection.prepareStatement(insertTeamsForUser);
			for (Team team : teams) {
				preparedStatement.setInt(1, user.getId());
				preparedStatement.setInt(2, team.getId());
				preparedStatement.addBatch();
			}
			preparedStatement.executeBatch();
			connection.commit();
			return true;
		} catch (SQLException throwables) {
			throwables.printStackTrace();
			rollBack(connection);
			throw new DBException("can not setTeamsForUser", throwables);
		} finally {
			close(preparedStatement);
			close(connection);
		}
	}

	public List<Team> getUserTeams(User user) throws DBException {
		List<Team> teams = new ArrayList<>();
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		String findTeam = "SELECT * FROM users_teams WHERE user_id=?";
		try {
			connection = getConnection();
			preparedStatement = connection.prepareStatement(findTeam);
			preparedStatement.setInt(1, user.getId());
			ResultSet resultSetTeams = preparedStatement.executeQuery();
			while (resultSetTeams.next()) {
				Team team = getTeam(connection, resultSetTeams.getInt(2));
				teams.add(team);
			}
			connection.commit();
		} catch (SQLException throwables) {
			throwables.printStackTrace();
			rollBack(connection);
			throw new DBException("can not get UserTeams", throwables);
		} finally {
			close(preparedStatement);
			close(connection);
		}
		return teams;
	}

	private Team getTeam(Connection con, int teams_id) {
		String findTeam = "SELECT * from teams where id=?";
		Team team = new Team();
		try (PreparedStatement preparedStatement = con.prepareStatement(findTeam)) {
			preparedStatement.setInt(1, teams_id);
			ResultSet resultSet = preparedStatement.executeQuery();
			resultSet.next();
			team.setId(resultSet.getInt(1));
			team.setName(resultSet.getString(2));
		} catch (SQLException throwables) {
			throwables.printStackTrace();
		}
		return team;
	}

	public boolean deleteTeam(Team team) throws DBException {
		Connection con = null;
		PreparedStatement preparedStatement = null;
		String deleteTeam = "DELETE FROM teams where name=?";
		try {
			con = getConnection();
			preparedStatement = con.prepareStatement(deleteTeam);
			preparedStatement.setString(1, team.getName());
			preparedStatement.execute();
			return true;
		} catch (SQLException throwables) {
			throwables.printStackTrace();
			throw new DBException("can not find this Team to delete", throwables);
		} finally {
			close(preparedStatement);
			close(con);
		}
	}

	public boolean updateTeam(Team team) throws DBException {
		Connection con = null;
		PreparedStatement preparedStatement = null;
		String updateTeam = "UPDATE teams SET name=? WHERE id=?";
		try {
			con = getConnection();
			preparedStatement = con.prepareStatement(updateTeam);
			preparedStatement.setString(1, team.getName());
			preparedStatement.setInt(2, team.getId());
			preparedStatement.executeUpdate();
			return true;
		} catch (SQLException throwables) {
			throwables.printStackTrace();
		} finally {
			close(preparedStatement);
			close(con);
		}
		return false;
	}

}